var express    = require('express'),
    app        = express(),
    bodyParser = require('body-parser'),
    path       = require('path');

//configuration
app.use(express.static(__dirname + '/../public'));
app.use(bodyParser.json());
console.log('config done');

//routing stuff
app.get('*', function(req, res) {
    res.sendFile(path.resolve('/public/index.html'));
});

app.post('/upload', function (req, res) {
    var parsedText = '';
    if (req.body.text) {
        data = parseText(req.body.text);
    }
    res.send(data);
});

function parseText(text) {
    var words = text.split(/\s+/);
    var wordCount = {};
    for (var i = 0; i < words.length; i++) {
        word = words[i].replace(/,|\.|\?|\!/, '').toLowerCase();
        if (isWord(word) === -1) {
            if (!wordCount[word]) {
                wordCount[word] = 1;
            } else {
                wordCount[word]++;  
            }
        }
    }
    var mostUsedWord = '';
    var numberOfUsages = 0;
    for (var word in wordCount) {
        if (wordCount[word] && wordCount[word] > numberOfUsages) {
            mostUsedWord = word;
            numberOfUsages = wordCount[word];
        }
    }
    var re = new RegExp(mostUsedWord, 'gi');
    var newText = text.replace(re, function(word) {
        return 'foo' + word + 'bar';
    });
    return { text: newText, word: mostUsedWord };
}

function isWord(word) {
    return word.search(/[^a-zA-Z']+/)
}

app.listen(8001);
console.log('Server listening to port 8001');
