'use strict';

angular.module('textMarker', [
  'ngRoute',
  'textMarker.main_view',
  'textMarker.file_uploader',
  'textMarker.file_change'
])