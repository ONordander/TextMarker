angular
    .module('textMarker.file_uploader', [])
    .service('fileUploader', function($http) {

        return function uploadFile(text) {
            var data = {};
            data.text = text;
            return $http.post('/upload', data, {
                header: { 'Content-Type': 'application/json' }
            })
            .then(function successCallback(response){
                return response;
            }, function errorCallBack(response) {
                return response;
            });
        }
    });