angular
    .module('textMarker.file_change', [])
    .directive('fileChange', function() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var fileChangeHandler = scope.$eval(attrs.fileChange);
          element.bind('change', fileChangeHandler);
        }
      };
    });