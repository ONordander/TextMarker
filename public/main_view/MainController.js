'use strict';
angular
    .module('textMarker.main_view', [])
    .controller('MainController', MainController);
MainController.$inject = ['$scope', 'fileUploader', '$parse'];
function MainController($scope, fileUploader, $parse) {
    var vm = this;
    vm.title = 'MainController';
    vm.parsedText = 'Here you will see your output';
    vm.mostUsedWord = '';
    activate();
    ////////////////
    function activate() {
        console.log('MainController activated')
    }

    vm.uploadFile = function() {
        var reader = new FileReader();
        reader.onload = function(evt) {
            fileUploader(reader.result).then(function(result) {
                vm.parsedText = result.data.text;
                vm.mostUsedWord = "Most used word was: " + result.data.word;
            });
        }
        var file = event.target.files[0];
        reader.readAsText(file);
    }
}