angular
    .module('textMarker')
    .config( function config($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: './main_view/main_view.html',
            controller: 'MainController',
            controllerAs: 'Ctrl'
        })
        .otherwise({redirectTo: '/'});
    });